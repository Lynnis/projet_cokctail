import React, { useState, useEffect } from 'react';
import { View, Text, Image, FlatList, TouchableOpacity, StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

const HomePage = ({ navigation }) => {
  const [cocktails, setCocktails] = useState([]);
  const [page, setPage] = useState(1);

  useEffect(() => {
    fetch(`https://www.thecocktaildb.com/api/json/v1/1/search.php?f=${String.fromCharCode(64 + page)}`)
        .then(response => response.json())
        .then(data => {
          setCocktails([...cocktails, ...data.drinks]);
        });
  }, [page]);

  const loadMoreCocktails = () => {
    setPage(page + 1);
  };

  return (
      <View style={styles.container}>
        <FlatList
            data={cocktails}
            keyExtractor={(item) => item.idDrink}
            onEndReached={loadMoreCocktails}
            onEndReachedThreshold={0.5}
            renderItem={({ item }) => (
                <TouchableOpacity onPress={() => navigation.navigate('Détail', { cocktail: item })}>
                  <View style={styles.cocktailItem}>
                    <Image source={{ uri: item.strDrinkThumb }} style={styles.cocktailImage} />
                    <Text style={styles.cocktailName}>{item.strDrink}</Text>
                  </View>
                </TouchableOpacity>
            )}
        />
      </View>
  );
};

const DetailPage = ({ route }) => {
  const { cocktail } = route.params;

  return (
      <View style={styles.container}>
        <Image source={{ uri: cocktail.strDrinkThumb }} style={styles.detailImage} />
        <Text style={styles.detailName}>{cocktail.strDrink}</Text>
        <Text style={styles.detailCategory}>Catégorie: {cocktail.strCategory}</Text>
        <Text style={styles.detailInstructions}>Instructions: {cocktail.strInstructions}</Text>
      </View>
  );
};

const App = () => {
  return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Accueil" component={HomePage} />
          <Stack.Screen name="Détail" component={DetailPage} />
        </Stack.Navigator>
      </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    padding: 10,
  },
  cocktailItem: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center',
  },
  cocktailImage: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 10,
  },
  cocktailName: {
    fontSize: 18,
  },
  detailImage: {
    width: '100%',
    height: 200,
  },
  detailName: {
    fontSize: 24,
    fontWeight: 'bold',
    marginVertical: 10,
  },
  detailCategory: {
    fontSize: 18,
    marginVertical: 5,
  },
  detailInstructions: {
    fontSize: 16,
    marginVertical: 5,
  },
});

export default App;